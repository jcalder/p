var express = require('express');
var path = require('path');
var logger = require('morgan');
var nodemailer = require('nodemailer');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer  = require('multer');
var subdomain = require('express-subdomain');
var node_env = process.env.PARAM1;

var dbConfig = require('./db');
var mongoose = require('mongoose');

var app = express();

if (node_env && node_env === 'production') {
    app.locals.base = 'prospector.io';
    mongoose.connect(dbConfig.productionUrl);
}else{
    app.locals.base = 'prospector.com:8081';
    mongoose.connect(dbConfig.url);
}

// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('trust proxy', 'loopback');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(multer({ dest: './uploads/'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(__dirname, '/uploads'));

// Configuring Passport
var passport = require('passport');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

if(node_env && node_env === 'production'){
  app.use(session({
      secret: 'theQuickBrownFox',
      store: new MongoStore({ mongooseConnection: mongoose.connection })
  }));
}else{
  app.use(session({secret: 'theQuickBrownFox',
                   saveUninitialized: true,
                   resave: true}));
}
app.use(passport.initialize());
app.use(passport.session());

app.use(function (req, res, next) {
  app.locals.user = req.user;
  next();
});

 // Using the flash middleware provided by connect-flash to store messages in session
 // and displaying in templates
var flash = require('connect-flash');
app.use(require('flash')());

// Initialize Passport
var initPassport = require('./passport/init');
initPassport(passport);

//Routes
var routes = require('./routes/index')(passport);
var publisher_routes = require('./routes/publisher')(passport);
var buyer_routes = require('./routes/buyer')(passport);
var api_routes = require('./routes/api')(passport);

app.use(subdomain('api', api_routes));
app.use(subdomain('publishers', publisher_routes));
app.use(subdomain('buyers', buyer_routes));
app.use('/', routes);

/// catch 404 and forward to error handler
if(process.env.PARAM1 === 'production'){
  app.use(function(req, res) {
      res.status(400);
      res.render('404.jade', {title: '404: File Not Found'});
  })
}else{
  app.use(function(req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      next(err);
  });
}

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

module.exports = app;