var url = require('url')

var Helper = function () {}

/**
 * Format URLs on signup to fit CORS origin specification
 * @param  {String} url Input URL string
 * @return {String}     Parsed and formatted URL string
 */
Helper.prototype.urlFormat = function (uri) {
	uri = uri.substr(0,7) === 'http://' || uri.substr(0,8) === 'https://'
		? uri
		: 'http://' + uri
	var parts = url.parse(uri)
	return parts.hostname
}

module.exports = new Helper()