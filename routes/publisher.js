/**
 * Required modules
 */
var express = require('express')
var async = require('async')
var ses = require('nodemailer-ses-transport')
var nodemailer = require('nodemailer')
var bCrypt = require('bcrypt-nodejs')
var router = express.Router()
var Blocks = require('../models/block')
var Publisher = require('../models/publisher')
var Submission = require('../models/submission')
var IP = require('../models/ip-logger')
var crypto = require('crypto');
var BlockModel = require('mongoose').model('Block');
var push = require('pushover-notifications')
var S3_KEY = 'AKIAJPISKDZOBNRUYU3Q';
var S3_SECRET = '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';
var mongoose = require('mongoose');

var accountRequest = {
    message: "",
    title: "Publisher signup request"
};

var p = new push( {
    user: 'uUyAXWZWwu4qdfhxdetRzyQ6gXPzhd',
    token: 'aRPSQ3KmED51eFRRBjtGPfxC4skacD'
});

/**
 * Expose routes
 */
module.exports = function(passport){

	/**
	 * Get root path.
	 * @param  {String}
	 * @param  {Function}
	 * @param {Function}
	 */
	router.get('/', isAuthenticated, function(req, res) {
		res.redirect('/home');
	});

	/**
	 * Get login path.
	 * @param {String}
	 * @param {Function}
	 */
	router.get('/login', function(req, res) {
    	res.render('publisher/index');
	});

	/**
	 * Post login path.
	 * @param {String}
	 * @param {Function} authenticate Authenticate user via Passportjs.
	 */
	router.post('/login', passport.authenticate('publisher_login', {
		successRedirect: '/home',
		failureRedirect: '/login',
		failureFlash : true
	}));

	/**
	 * Get signup path.
	 * @param {String}
	 * @param {Function}
	 */
	router.get('/signup', function(req, res){
		res.render('publisher/register');
	});

	/**
	 * Get signup path.
	 * @param {String}
	 * @param {Function}
	 */
	router.get('/signup-request', function(req, res){
		res.render('publisher/register-paused');
	});

	/**
	 * Post signup path.
	 * @param {String}
	 * @param {Function} authenticate Set user authentication.
	 */
	router.post('/signup', passport.authenticate('publisher_signup', {
		successRedirect: '/home',
		failureRedirect: '/signup',
		failureFlash : true
	}));

	router.post('/request-account', function(req, res){
		accountRequest.message = req.body.email
		p.send( accountRequest );
		req.flash("info", "Thank you! We've recieved your request. We'll be in touch shortly.")
		res.redirect('/signup')
	})

	/**
	 * Get forgot password path.
	 * @param {String}
	 * @param {Function}
	 */
	router.get('/forgot', function (req, res){
		res.render('publisher/forgot');
	});

	/**
	 * Post forgot password path.
	 * @param {String}
	 * @param {Function}
	 */
	router.post('/forgot', function (req, res, next) {
	  async.waterfall([
	    function(done) {
	      crypto.randomBytes(20, function(err, buf) {
	        var token = buf.toString('hex');
	        done(err, token);
	      });
	    },
	    function(token, done) {
	      Publisher.findOne({ email: req.body.email }, function(err, user) {
	        if (!user) {
	          req.flash('danger', 'No account with that email address exists.');
	          return res.redirect('/forgot');
	        }

	        user.resetPasswordToken = token;

	        user.save(function(err) {
	          done(err, token, user);
	        });
	      });
	    },
	    function(token, user, done) {
	      var smtpTransport = nodemailer.createTransport(ses({
			  accessKeyId: S3_KEY,
			  secretAccessKey: S3_SECRET,
			  region: 'us-west-2'
	  	  }));
	      var mailOptions = {
	        to: user.email,
	        from: 'passwordreset@prospector.io',
	        subject: 'Node.js Password Reset',
	        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
	          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
	          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
	          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
	      };
	      smtpTransport.sendMail(mailOptions, function(err) {
	        req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
	        done(err, 'done');
	      });
	    }
	  ], function(err) {
	    if (err) return next(err);
	    res.redirect('/forgot');
	  });
	});

	/**
	 * Post reset password :token path.
	 * @param {String}
	 * @param {Function}
	 */
	router.post('/reset/:token', function(req, res) {
      Publisher.findOne({ resetPasswordToken: req.params.token }, function(err, user) {
        if (!user) {
          req.flash('danger', 'Password reset token is invalid or has expired.');
          res.redirect('/');
        }

        user.password = createHash(req.body.password);

        user.save(function(err) {
          req.logIn(user, function(err) {
            done(err, user);
            req.flash('success', 'Success! Your password has been changed.');
            res.redirect('/')
          });
        });
      });
	});

	/**
	 * Get reset password path (:token).
	 * @param {String}
	 * @param {Function}
	 */
	router.get('/reset/:token', function(req, res) {
	  Publisher.findOne({ resetPasswordToken: req.params.token }, function(err, user) {
	    if (!user) {
	      req.flash('danger', 'Password reset token is invalid or has expired.');
	      return res.redirect('/forgot');
	    }
	    res.render('publisher/reset', {
	      user: req.user
	    });
	  });
	});

	/**
	 * Get email verification path (:token).
	 * @param {String}
	 * @param {Function}
	 */
	router.get('/verify/:token',function(req,res){
		Publisher.findOne({verifyToken: req.params.token}, function (err, user){
			if(!user){
				req.flash('danger', 'Cannot find user.');
	      		return res.redirect('/login');
			}
			user.verified = true;
	        user.save();
			req.logIn(user, function(err) {
				req.flash('success', 'Account successfully verified.');
	            res.redirect('/');
	        });
		});
	});

	/**
	 * Resend email verification.
	 * @param  {function}
	 * @param  {function}
	 */
	router.get('/verify', isAuthenticated, function(req,res){
		var smtpTransport = nodemailer.createTransport(ses({
			  accessKeyId: S3_KEY,
			  secretAccessKey: S3_SECRET,
			  region: 'us-west-2'
	  	  }));
	      var mailOptions = {
	        to: req.user.email,
	        from: 'info@prospector.io',
	        subject: 'Account Verification',
	        text: 'Hello,\n\n' +
	          'You are receiving this because you have requested account verification. Please follow this link to verify your account: \n\n' +
	          'http://' + req.headers.host + '/verify/' + req.user.verifyToken + '\n\n'
	      };
	      smtpTransport.sendMail(mailOptions, function(err) {
	        req.flash('success', 'Your verification email has been sent to your inbox.');
	      });
	      res.redirect(req.headers.referer)
	})

	/**
	 * Get home path.
	 * @param {String}
	 * @param {Function}
	 * @param {Function}
	 */
	router.get('/home', isAuthenticated, function(req, res){
		var dayTotal = 0
		var weekTotal = 0
		var monthTotal = 0
		Submission.aggregate([
		    // First total per day. Rounding dates with math here
		    {"$match":{
		    	"_publisherId": req.user._id
		    }},
		    { "$group": {
		        "_id": {
		            "$add": [
		                { "$subtract": [
		                    { "$subtract": [ "$createdAt", new Date(0) ] },
		                    { "$mod": [
		                        { "$subtract": [ "$createdAt", new Date(0) ] },
		                        1000 * 60 * 60 * 24
		                    ]}
		                ]},
		                new Date(0)
		            ]
		        },
		        "week": { "$first": { "$week": "$createdAt" } },
		        "month": { "$first": { "$month": "$createdAt" } },
		        "total": { "$sum": "$bid" }
		    }},

		    // Then group by week
		    { "$group": {
		        "_id": "$week",
		        "month": { "$first": "$month" },
		        "days": {
		            "$push": {
		                "day": "$_id",
		                "total": "$total"
		            }
		        },
		        "total": { "$sum": "$total" }
		    }},

		    // Then group by month
		    { "$group": {
		        "_id": "$month",
		        "weeks": {
		            "$push": {
		                "week": "$_id",
		                "total": "$total",
		                "days": "$days"
		            }
		        },
		        "total": { "$sum": "$total" }
		    }}
		], function(err, match){
			console.log(match)
			try{
				dayTotal = "$"+(match[0].weeks[0].days[0].total/100*.6).toFixed(2)
				weekTotal = "$"+(match[0].weeks[0].total/100*.6).toFixed(2)
				monthTotal = "$"+(match[0].total/100*.6).toFixed(2)
			} catch(e){
				dayTotal = '$0'
				weekTotal = '$0'
				monthTotal = '$0'
			}
		})
		var total = 0
		Submission.aggregate([
			{"$match":{
		    	"_publisherId": req.user._id
		    }},
			{
				$group: {
					'_id': req.user._id,
					total:{$sum:'$bid'}
				}
			}
		], function(err, match){
			try{
				total = "$"+(match[0].total/100).toFixed(2)
			} catch (e) {
				total = '$0'
			}
			res.render('publisher/home', {
				user: req.user,
				dayTotalEarnings: dayTotal,
				weekTotalEarnings: weekTotal,
				monthTotalEarnings: monthTotal,
				totalEarnings: total,
			});
		})
	});

	/**
	 * Get signout path.
	 * @param {String}
	 * @param {Function}
	 */
	router.get('/signout', function (req, res) {
		req.logout();
		res.redirect('/');
	});

	/**
	 * Get account path.
	 * @param {String}
	 * @param {Function}
	 * @param {Function}
	 */
	router.get('/account', isAuthenticated, function(req, res){
		res.render('publisher/profile', { balance: "$"+(req.user.credit/100).toFixed(2) });
	});

	/**
	 * Get all blocks path.
	 * @param {String}
	 * @param {Function}
	 * @param {Function}
	 */
	router.get('/blocks', isAuthenticated, isVerified, function (req, res){
		var query = Blocks.findOne({creator: req.user._id});
		query.exec(function(err,block){
			var data = []
			for(col in block){
				console.log('col', col)
				var d = {"data": col}
				data.push(d)
			}
			console.log('data', data)
			res.render('publisher/blocks/index', {blocks: block, columns: data});
		})
	});

	router.get('/blocks-table', isAuthenticated, isVerified, function (req, res){
		var query = Blocks.find({creator: req.user._id}, {name:1, description: 1, status: 1, _id:1});
		query.exec(function(err,blocks){
			var table = {
			  recordsTotal: blocks.length,
			  recordsFiltered: blocks.length,
			  data: blocks
			}

		  	res.json(table);
		});
	})

	router.get('/get-code/:id', isAuthenticated, isVerified, function (req, res){
		var codeBlock = "<script async data-id='" + req.params.id + "' data-name='prospectscript' src='http://prospector.io/javascripts/processprospector.js'></script>";
		res.render('publisher/blocks/get-code', {code:codeBlock})
	})

	/**
	 * Get create block path.
	 * @param {String}
	 * @param {Function}
	 * @param {Function}
	 */
	router.get('/block/new', isAuthenticated, isVerified, function (req, res){
		var id = mongoose.Types.ObjectId();
		var codeBlock = "<script async data-id='" + id + "' data-name='prospectscript' src='http://prospector.io/javascripts/processprospector.js'></script>";;
		res.render('publisher/blocks/new', {code: codeBlock, blockid: id});
	});

	/**
	 * Get block by :id.
	 * @param {String}
	 * @param {Function}
	 * @param {Function}
	 */
	router.get('/block/:id', isAuthenticated, function (req, res){
		var query = Blocks.findOne({'_id': req.params.id, creator: req.user._id});
		query.exec(function(err,block){
			if(err) return handleError(err);
			res.render('publisher/blocks/block',{
				block: block
			});
		});
	});

	/**
	 * Post create block path.
	 * @param {String}
	 * @param {Function}
	 * @param {Function}
	 */
	router.post('/block/new/:id', isAuthenticated, isVerified, function (req, res){
	  var block;
	  var id = mongoose.Types.ObjectId(req.params.id);
	  block = new Blocks({
	  	_id: id,
	    name: req.body.name,
	    status: 'active',
	    description: req.body.description,
	    creator: req.user._id
	  });
	  block.save();
	  res.block = block;
	  Publisher.update(
	  	{_id: req.user._id},
	  	{$push: {blocks:[block._id]}},
	  	{safe:true, upsert:true},
	  	function(err, model) {
	    	console.log(err);
	  });
	  req.flash('success', 'Saved new block!');
      res.redirect('/blocks');
	});

	router.get('/tour-complete', function(req,res){
		Publisher.update(
			{ '_id': req.user._id },
			{ $set: { takenTour: true } },
		    function(err, user) {
				console.log("USER",user)
				res.end("Finished")
		});
	})

	/**
	 * Block implementation guide.
	 * @param {String}
	 * @param {Function}
	 */
	router.get('/block-implementation', function (req, res){
		res.render('publisher/blocks/implementation');
	});

	router.get('/admin', isAuthenticated, function(req, res){
		if(req.user.email === 'jordancalder@gmail.com'){
			Publisher.count({},function(err,c){
				Publisher.find({},function(err,p){
					IP.aggregate([{$group:{_id:{ $dayOfYear: "$date"},visits: { $sum: '$visits' }}},{$sort:{_id:-1}},{$limit:30}], function(err, result){
						console.log(result)
						var labels = []
						var vals = []
						for(var i = 0; i < result.length; i++){
							console.log(i)
							labels.unshift(result[i]['_id'])
							vals.unshift(result[i].visits)
						}
						res.render('publisher/admin', {count: c, publishers: p, lab: JSON.stringify(labels), val: JSON.stringify(vals)})
					})
				})
			})
		} else {
			res.redirect('/')
		}
	})

	return router;
}

/**
 * Check if user is properly authenticated to view certain pages.
 * @param  {String}
 * @param  {String}
 * @param  {Function}
 * @return {Function}
 */
var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated()) return next();
	res.redirect('/login');
}

/**
 * Check if the current user has a verified email.
 * @param  {String}
 * @param  {String}
 * @param  {Function}
 * @return {Function}
 */
var isVerified = function (req, res, next){
	if(req.user.verified) return next();
	req.flash('info', "This email has not been verified. <a href='/verify' class='alert-link'>Resend</a> verification email.");
	res.redirect(req.headers.referer);
}

/**
 * Create password hash.
 * @param  {String} password User created password.
 * @return {String}          Hashed password.
 */
var createHash = function(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}