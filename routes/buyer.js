/**
 * Require needed modules
 */
var express = require('express')
var router = express.Router()
var async = require('async')
var nodemailer = require('nodemailer')
var ses = require('nodemailer-ses-transport')
var bCrypt = require('bcrypt-nodejs')
var crypto = require('crypto')
var F = require('../models/form')
var stripe = require('stripe')('sk_live_sNFrRKwy1YmYbZNMkVgNo94g')
var Buyer = require('../models/buyer')
var Submission = require('../models/submission')
var SubModel = require('mongoose').model('Submission');
var geoip = require('geoip-lite');
var push = require('pushover-notifications')

var accountRequest = {
    message: "",
    title: "Buyer signup request"
};

var p = new push( {
    user: 'uUyAXWZWwu4qdfhxdetRzyQ6gXPzhd',
    token: 'aRPSQ3KmED51eFRRBjtGPfxC4skacD'
});

/**
 * Buyer variables
 */
var S3_KEY = 'AKIAJPISKDZOBNRUYU3Q'
	, S3_SECRET = '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'
	, S3_BUCKET = 'prospectoruploads'
	, knox = require('knox').createClient({
	    key: S3_KEY,
	    secret: S3_SECRET,
	    bucket: S3_BUCKET
	});

/**
 * Expose routes
 */
module.exports = function(passport){

	/**
	 * Get root path.
	 * @param  {String}
	 * @param  {Function}
	 * @return {Function}
	 */
	router.get('/', isAuthenticated, function(req, res) {
		res.redirect('/home');
	});

	/**
	 * Get login path.
	 * @param  {String}
	 * @param  {Function}
	 */
	router.get('/login', function(req, res) {
		if(req.isAuthenticated())
			res.redirect('/home');
		res.render('buyer/index');
	});

	/**
	 * Post login path.
	 * @type {String}
	 * @param {Function} authenticate Authenticate user through passport package.
	 */
	router.post('/login', passport.authenticate('buyer_login', {
		successRedirect: '/home',
		failureRedirect: '/login',
		failureFlash : true
	}));

	/**
	 * Get signup path.
	 * @param  {String}
	 * @param  {Function}
	 */
	router.get('/signup', function(req, res){
		if(req.isAuthenticated())
			res.redirect('/home');
		res.render('buyer/register');
	});

	/**
	 * Get signup path.
	 * @param  {String}
	 * @param  {Function}
	 */
	router.get('/signup-test', function(req, res){
		if(req.isAuthenticated())
			res.redirect('/home');
		res.render('buyer/register-paused');
	});

	/**
	 * Post signup path.
	 * @type {String}
	 * @param {Function} authenticate Create user and user authentication.
	 */
	router.post('/signup', passport.authenticate('buyer_signup', {
		successRedirect: '/home',
		failureRedirect: '/signup',
		failureFlash : true
	}));

	router.post('/request-account', function(req, res){
		accountRequest.message = req.body.email
		p.send( accountRequest );
		req.flash("Thank you! We've recieved your request. We'll be in touch shortly.")
		res.redirect('/signup')
	})

	router.get('/tour-complete', function(req,res){
		Buyer.findOne({ '_id': req.user._id }, function(err, user) {
			user.takenTour = true
			user.save(function(err){
				res.end("Finished")
			})
		});
	})

	/**
	 * Get forgotten password path.
	 * @param  {String}
	 * @param  {Function}
	 */
	router.get('/forgot', function (req, res){
		res.render('buyer/forgot');
	});

	/**
	 * Post forgotten password path.
	 * @param  {String}
	 * @param  {Function}
	 * @param  {Function}
	 * @param  {Function}
	 * @param  {Function}
	 * @param  {Function}
	 */
	router.post('/forgot', function (req, res, next) {
	  async.waterfall([
	    function(done) {
	      crypto.randomBytes(20, function(err, buf) {
	        var token = buf.toString('hex');
	        done(err, token);
	      });
	    },
	    function(token, done) {
	      Buyer.findOne({ email: req.body.email }, function(err, user) {
	        if (!user) {
	          req.flash('danger', 'No account with that email address exists.');
	          return res.redirect('/forgot');
	        }

	        user.resetPasswordToken = token;

	        user.save(function(err) {
	          done(err, token, user);
	        });
	      });
	    },
	    function(token, user, done) {
	      var smtpTransport = nodemailer.createTransport(ses({
			  accessKeyId: S3_KEY,
			  secretAccessKey: S3_SECRET,
			  region: 'us-west-2'
	  	  }));
	      var mailOptions = {
	        to: user.email,
	        from: 'info@prospector.io',
	        subject: 'Node.js Password Reset',
	        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
	          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
	          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
	          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
	      };
	      smtpTransport.sendMail(mailOptions, function(err) {
	        req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
	        done(err, 'done');
	      });
	    }
	  ], function(err) {
	    if (err) return next(err);
	    res.redirect('/forgot');
	  });
	});

	/**
	 * Post password reset path
	 * @param  {String}
	 * @param  {Function}
	 * @param  {Function}
	 * @param  {Function}
	 */
	router.post('/reset/:token', function(req, res) {
      Buyer.findOne({ resetPasswordToken: req.params.token }, function(err, user) {
        if (!user) {
          req.flash('danger', 'Password reset token is invalid or has expired.');
          res.redirect('/');
        }

        user.password = createHash(req.body.password);

        user.save(function(err) {
          req.logIn(user, function(err) {
            done(err, user);
            req.flash('success', 'Success! Your password has been changed.');
            res.redirect('/')
          });
        });
      });
	});

	/**
	 * Get reset token path for password reset.
	 * @param  {String}
	 * @param  {function}
	 */
	router.get('/reset/:token', function(req, res) {
	  Buyer.findOne({ resetPasswordToken: req.params.token }, function(err, user) {
	    if (!user) {
	      req.flash('danger', 'Password reset token is invalid or has expired.');
	      return res.redirect('/forgot');
	    }
	    res.render('buyer/reset', {
	      user: req.user
	    });
	  });
	});

	/**
	 * Get email verification token path.
	 * @param  {String}
	 * @param  {Function}
	 */
	router.get('/verify/:token',function(req,res){
		Buyer.findOne({verifyToken: req.params.token}, function(err, user){
			if(!user){
				req.flash('danger', 'Cannot find user.');
	      		return res.redirect('/login');
			}
			user.verified = true;

	        user.save();
			req.logIn(user, function(err) {
				req.flash('success', 'Account successfully verified.');
	            res.redirect('/');
	        });
		});
	});

	/**
	 * Resend email verification.
	 * @param  {function}
	 * @param  {function}
	 */
	router.get('/verify', isAuthenticated, function(req,res){
		var smtpTransport = nodemailer.createTransport(ses({
			  accessKeyId: S3_KEY,
			  secretAccessKey: S3_SECRET,
			  region: 'us-west-2'
	  	  }));
	      var mailOptions = {
	        to: req.user.email,
	        from: 'info@prospector.io',
	        subject: 'Account Verification',
	        text: 'Hello,\n\n' +
	          'You are receiving this because you have requested account verification. Please follow this link to verify your account: \n\n' +
	          'http://' + req.headers.host + '/verify/' + req.user.verifyToken + '\n\n'
	      };
	      smtpTransport.sendMail(mailOptions, function(err) {
	        req.flash('success', 'Your verification email has been sent to your inbox.');
	      });
	      res.redirect(req.headers.referer)
	})

	/**
	 * Get home path.
	 * @param  {String}
	 * @param  {Function}
	 * @param {Function}
	 */
	router.get('/home', isAuthenticated, function(req, res){
		Submission.find({_buyerId: req.user._id},function(err,c){
			var ac = req.user.forms.reduce(function(a,b){
			    if(b.fields.length > 0 && b.active) return a + 1
			    return a + 0
			}, 0)
			var d = new Date();
			d.setHours(0,0,0,0);
			console.log(d)
			var todaySub = c.reduce(function(a, b){
				console.log(b)
				if(Date.parse(b.createdAt) > Date.parse(d)) return a + 1
				return a + 0
			}, 0)
			res.render('buyer/home', {
				user: req.user,
				activeCampaigns: ac,
				balance: "$"+(req.user.credit/100).toFixed(2),
				submissionCount: c.length,
				todaySubmissions: todaySub
			});
		})
	});

	/**
	 * Get account details path.
	 * @param  {String}
	 * @param  {Function}
	 * @param {Function}
	 */
	router.get('/account', isAuthenticated, function(req, res){
		res.render('buyer/profile', { balance: "$"+(req.user.credit/100).toFixed(2) });
	});

	/**
	 * Get signout path.
	 * @param  {String}
	 * @param  {Function}
	 */
	router.get('/signout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	/**
	 * Get campaigns list path.
	 * @param  {String}
	 * @param  {Function}
	 * @param {Function}
	 * @param {Function}
	 */
	router.get('/campaigns', isAuthenticated, isVerified, function (req,res){
		res.render('buyer/forms');
	});

	router.get('/campaigns-table', isAuthenticated, isVerified, function(req, res){
		var query = Buyer.findOne({'_id':req.user._id});
		query.exec(function(err, buyer){
			if(err) console.log(err)
			var d = []
			for(var i = 0; i < buyer.forms.length; i++){
				d.push({
					_id: buyer.forms[i]._id,
					name: buyer.forms[i].cta,
					status: buyer.forms[i].active ? 'active' : 'inactive',
					bid: (buyer.forms[i].bid/100).toFixed(2),
					fields: buyer.forms[i].fields.length
				})
			}
			var table = {
				  recordsTotal: d.length,
				  recordsFiltered: d.length,
				  data: d
				}

		  	res.json(table);
		})
	})

	router.get('/toggle-active/:id', isAuthenticated, isVerified, function(req, res){
		var query = Buyer.findOne({'_id': req.user._id})
		query.exec(function(err,buyer){
			var active = buyer.forms.id(req.params.id).active
			buyer.forms.id(req.params.id).active = active
				? false
				: true
			buyer.save(function(err){
				res.redirect('/campaigns')
			})
		})
	})

	/**
	 * Get new campaign creation path.
	 * @param  {String}
	 * @param  {Function}
	 * @param {Function}
	 */
	router.get('/campaign/new', isAuthenticated, function(req, res) {
		var query = Buyer.findOne({'_id':req.user._id});
		query.exec(function(err,buyer){
			var f = new F();
			buyer.forms.push(f);
			buyer.save();
			res.redirect('/campaign/' + f._id);
		});
	});

	/**
	 * Get campaign by :id path.
	 * @param  {String}
	 * @param  {Function}
	 * @param  {Function}
	 */
	router.get('/campaign/:id', isAuthenticated, function (req, res){
		var query = Buyer.findOne({'_id': req.user._id});
		query.exec(function(err,buyer){
			if(err) console.log(err);
			var formData;
			if(buyer){
				formData = buyer.forms.id(req.params.id);
			}else{
				formData = "[]";
			}
			var bid = formData
				? (formData.bid/100).toFixed(2)
				: null
			res.render('buyer/campaign',{
				fdata: formData,
				bid: bid,
			});
		});
	});

	/**
	 * Update campaign by :id
	 * @param  {String}
	 * @param  {Function}
	 * @param  {Function}
	 */
	router.post('/campaign/:id', isAuthenticated, function (req, res){
		var query = Buyer.findOne({'_id': req.user._id});
		query.exec(function(err,buyer){
			if(err) console.log(err);
			if(req.body.cta) buyer.forms.id(req.params.id).cta = req.body.cta;
			if(req.body.copy) buyer.forms.id(req.params.id).copy = req.body.copy;
			if(req.files.logo) buyer.forms.id(req.params.id).logo = req.files.logo.path;
			buyer.forms.id(req.params.id).bid = (req.body.bid && req.body.bid > 0.15)
				? (req.body.bid*100).toFixed(0)
				: 15
			if(req.body.keywords) buyer.forms.id(req.params.id).keywords = req.body.keywords.split(',');
			buyer.forms.id(req.params.id).latlong = [],
			buyer.forms.id(req.params.id).country = "";
			buyer.forms.id(req.params.id).region = "";
			buyer.forms.id(req.params.id).city = "";
			if(req.body.loc == 'us') buyer.forms.id(req.params.id).country = "US";
			if(req.body.loc == 'state') buyer.forms.id(req.params.id).region = req.body.state;
			if(req.body.loc == 'custom') buyer.forms.id(req.params.id).latlong = [req.body.lat, req.body.lon];
			buyer.forms.id(req.params.id).active = true;
			buyer.save(function (err) {
				if (!err) {
					if(req.files.logo){
						knox.putFile(req.files.logo.path, req.files.logo.path, {'Content-Type': 'image/jpeg', 'x-amz-acl': 'public-read'}, function(err, result) {
						    if (200 == result.statusCode) { console.log('Uploaded to mazon S3'); }
						    else { console.log('Failed to upload file to Amazon S3'); }
						});
						res.send(console.dir(req.files));
					}
				} else {
				  return console.log(err);
				}
			});
			if(buyer.forms.id(req.params.id).fields[0]){
				res.redirect('/campaigns');
			}else{
				res.redirect('/formbuilder/'+req.params.id)
			}
		});
	});

	/**
	 * Get formbuilder by :id.
	 * @param  {String}
	 * @param  {Function}
	 * @param  {Function}
	 */
	router.get('/formbuilder/:id', isAuthenticated, function(req, res){
		var query = Buyer.findOne({'_id': req.user._id});
		query.exec(function(err,buyer){
			if(err) console.log(err);
			var fieldData;
			if(buyer.forms.id(req.params.id).fields[0]){
				req.flash("danger", "Cannot modify forms once they have been saved.");
				console.dir(req.flash)
				res.redirect('/campaign/'+req.params.id)
				//fieldData = JSON.stringify(buyer.forms.id(req.params.id).fields[0].fields);
			}else{
				fieldData = "[]";
				res.render('buyer/form-builder',{
					f: fieldData,
				});
			}
		});
	});

	/**
	 * Update/Save formbuilder changes. Path in formbuilder.js config.
	 * @param  {String}
	 * @param  {Function}
	 * @param {Function}
	 */
	router.post('/fb/update', isAuthenticated, function(req, res){
		var url = req.headers.referer;
		var formId = url.substr(url.lastIndexOf('/') + 1);
		var query = Buyer.findOne({'_id': req.user._id});
		query.exec(function(err,buyer){
			if(err) console.log(err);
			buyer.forms.id(formId).fields = {
				fields: req.body.fields,
			};
			buyer.save();
		});
	});

	/**
	 * Get campaigns for viewing submissions by campaign.
	 * @param  {String}
	 * @param  {Function}
	 * @param {Function}
	 */
	router.get('/submissions', isAuthenticated, isVerified, function (req, res){
		var query = Buyer.findOne({'_id':req.user._id});
		query.exec(function(err,buyer){
			if(err) console.log(err);
			res.render('buyer/submissions',{forms:buyer.forms});
		});
	})

	/**
	 * Get all submissions for a given campaign.
	 * @param  {String}
	 * @param  {Function}
	 * @param  {Function}
	 * @param  {Function}
	 */
	router.get('/submissions/:id', isAuthenticated, isVerified, function (req, res){
		var query = Submission.findOne({'_formId': req.params.id});
		query.exec(function(err, sub){
			console.log(sub)
			if(err) console.log(err)
			if(sub){
				var dataCol = sub.fields[0].fields
				var data = []
				for(col in dataCol){
					var d = {"data": col}
					data.push(d)
				}
				console.log('Data: ', data);
				res.render('buyer/campaign-submissions', {fields: sub, columns: data, sid: req.params.id})
			}else{
				res.render('buyer/campaign-submissions', {message: req.flash("There have been no submissions to this campaign.")})
			}
		})
	})

	/**
	 * Helper router for dataTables to display the submissions.
	 * @param  {String}
	 * @param  {Function}
	 * @param  {Function}
	 * @param  {Function}
	 */
	router.get('/submissions-table/:id', isAuthenticated, isVerified, function(req, res){
		var query = Submission.find({'_formId': req.params.id}, {fields: 1});
		query.exec(function(err,submissions){
			var d = []
			for(sub in submissions){
				if(submissions[sub].fields[0].fields) d.push(submissions[sub].fields[0].fields)
			}
			var table = {
				  recordsTotal: d.length,
				  recordsFiltered: d.length,
				  data: d
				}

		  	res.json(table);
		});
	})

	/**
	 * Get path to fund account via Stripe.
	 * @param  {String}
	 * @param  {Function}
	 * @param {Function}
	 */
	router.get('/fund', isAuthenticated, function (req, res){
		var months = [
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December'
		]
		var d = new Date()
		var year = d.getFullYear()
		var years = []
		for(var i = 0; i < 10; i++){
			years.push(year)
			year++
		}
		res.render('buyer/fund', {months: months, years: years});
	});

	/**
	 * Charge user and add fund balance in account.
	 * @param  {String}
	 * @param  {Function}
	 * @return {Function}
	 */
	router.post('/charge', isAuthenticated, function (req, res) {
		if (!req.body || !req.body.token) return res.send({ok: false, message: 'error'});
		var amountPennies = req.body.amount * 100
		chargeCard(req.body.token, amountPennies, res);

		var query = Buyer.findOne({'_id': req.user._id});
		query.exec(function(err,buyer){
			buyer.credit += amountPennies
			buyer.save();
		});
	});

	router.get('/admin', isAuthenticated, function(req, res){
		if(req.user.email === 'jordancalder@gmail.com'){
			Buyer.count({},function(err,c){
				Buyer.find({}, function(err,b){
					res.render('buyer/admin', {count: c, buyers: b})
				})
			})
		} else {
			res.redirect('/')
		}
	});

	return router;
}

/**
 * Check if user is properly authenticated to view certain pages.
 * @param  {String}
 * @param  {String}
 * @param  {Function}
 * @return {Function}
 */
var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated()) return next();
	res.redirect('/login');
}

/**
 * Check if the current user has a verified email.
 * @param  {String}
 * @param  {String}
 * @param  {Function}
 * @return {Function}
 */
var isVerified = function (req, res, next){
	if(req.user.verified) return next();
	req.flash('info', "This email has not been verified. <a href='#' class='alert-link'>Resend</a> verification email.");
	res.redirect(req.headers.referer);
}

/**
 * Create hash for passwords.
 * @param {String} password User created string.
 * @return {String}
 */
var createHash = function(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}

/**
 * Send a charge request to Stripe API for user.
 * @param  {String}	token Security token object.
 * @param  {String}	amt Amount to be charged (in cents)
 * @param  {String}	res Response object
 */
var chargeCard = function(token, amt, res){
	stripe.charges.create({
		amount: amt,
		currency: 'usd',
		card: token,
		description: 'Test charge from ' + (new Date()),
	}, function(err, charge) {
		if (err) {
			res.send({ok: false, message: 'There was a problem processing your card (error: ' + JSON.stringify(err) + ')'});
		} else {
			res.send({ok: true, message: 'Your transaction completed!'});
		}
	});
}