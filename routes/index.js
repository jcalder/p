var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');
var ses = require('nodemailer-ses-transport');
var S3_KEY = 'AKIAJPISKDZOBNRUYU3Q';
var S3_SECRET = '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';

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.redirect('/');
}

module.exports = function(passport){

	/**
	 * GET home page.
	 */
	router.get('/', function(req, res) {
		(req.query.a && req.query.a === 'pub')
			? res.render('publisher-index')
	    	: res.render('index');
	});

	/**
	 * GET about page.
	 */
	router.get('/about', function(req, res){
		res.render('about');
	});

	/**
	 * GET contact page.
	 */
	router.get('/contact', function(req, res){
		res.render('contact');
	});

	/**
	 * POST contact page.
	 */
	router.post('/contact', function(req, res){
		var smtpTransport = nodemailer.createTransport(ses({
          accessKeyId: S3_KEY,
          secretAccessKey: S3_SECRET,
          region: 'us-west-2'
        }));
        var mailOptions = {
            to: 'jordan@prospector.io',
            from: 'info@prospector.io',
            subject: 'Contact Form Submission',
            text: 'Message details:\n\n' +
                req.body.name + '\n\n' +
				req.body.email + '\n\n' +
                req.body.message
        };
        smtpTransport.sendMail(mailOptions, function(err) {
            req.flash('success', 'Thank you! We have received your message and will be in touch shortly.');
        });
        res.render('contact', {message: 'Thank you! We have received your message and will be in touch shortly.'});
	})

	return router;
}