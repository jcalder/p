/**
 * Required modules
 */
var express = require('express')
var router = express.Router()
var F = require('../models/form')
var token = require('../models/token')
var submission = require('../models/submission')
var buyer = require('../models/buyer')
var publisher = require('../models/publisher')
var block = require('../models/block')
var ipLogger = require('../models/ip-logger')
var geoip = require('geoip-lite')
var cors = require('cors')
var geolib = require('geolib')

/**
 * Expose API routes
 */
module.exports = function(passport){

	router.get('/iframe/:blockId', function(req, res){
		console.log('origin: ', req.header('Origin'))
		console.log('origin2: ', req.headers.origin)
		console.log('referrer: ', req.header('Referer'))
		console.log('referrer2: ', req.headers.referer)
		var ip = req.header('X-Forwarded-For')

		var date = new Date()
		var start = date.setHours(0,0,0,0);
		date.setDate(date.getDate() + 1)
		ipLogger.findOne({ip:ip, date: {$lt: new Date(date), $gte: new Date(start)}}, function(err, entry){
			if(entry === null){
				var expiry = new token();
				expiry.save();

				getFormByGeo(req).then(function(data){
					return data
				}, function(err){
					return getFormNoGeo(req)
				}).then(function(f){
					if(f){
						var id = f._id
						var f = f.forms;
						f['token'] = expiry._id;
						f['blockId'] = req.params.blockId;
						f['buyerId'] = id;
						res.render('api/iframe', {form : f, fields: f.fields[0].fields});

					}else{
						res.end("Form not found");
					}
				}, function(err){
					res.end(err)
				})
				var log = new ipLogger({
					ip: ip,
					origin: req.header('Referer'),
					visits: 1
				}).save()
			} else {
				entry.visits += 1
				entry.save()
				res.end("Too many requests from this ip today.")
			}
		})
	})

	router.get('/example-iframe', function(req, res){
		res.render('api/example-iframe')
	})

	/**
	 * Get a form, validate origin by blockId.
	 */
	router.get('/form/:blockId', cors(corsOptionsDelegate), function(req, res) {
		var ip = req.header('X-Forwarded-For')

		var date = new Date()
		date.setDate(date.getDate() + 1)
		ipLogger.findOne({ip:ip, date: {$lt: new Date(date)}}, function(err, entry){
			if(entry === null){
				var expiry = new token();
				expiry.save();

				var callback = req.query.callback;

				getFormByGeo(req).then(function(data){
					return data
				}, function(err){
					return getFormNoGeo(req)
				}).then(function(f){
					if(f){
						var id = f._id
						var f = f.forms;
						f['token'] = expiry;
						f['blockId'] = req.params.blockId;
						f['buyerId'] = id;
						if(err) console.log(err);
						if (callback) {
							console.log('responding...', callback)
						    res.setHeader('Content-Type', 'text/javascript');
						    res.write(callback + '(' + JSON.stringify(f) + ')');
						    res.end();
						} else {
						    res.write("Callback not set");
						    res.end();
						}
					}else{
						res.end("Form not found");
					}
				})
				var log = new ipLogger({
					ip: ip,
					origin: req.header('Origin'),
					visits: 1
				}).save()
			} else {
				entry.visits += 1
				entry.save()
				res.end("Too many requests from this ip today.")
			}
		})
	});

	/**
	 * Get a form for localhost testing.
	 */
	router.get('/form-test/:blockId', cors(), function(req, res) {
		var ip = process.env.PARAM1 === 'production'
		? req.ip
		: '67.186.210.17';

		var date = new Date()
		date.setDate(date.getDate() + 1)
		ipLogger.findOne({ip:ip, date: {$lt: new Date(date)}}, function(err, entry){
			console.log(entry)
			console.log(new Date())
			if(entry === null){
				var expiry = new token();
				expiry.save();

				var callback = req.query.callback;

				getFormByGeo(req).then(function(data){
					return data
				}, function(err){
					return getFormNoGeo(req)
				}).then(function(f){
					if(f){
						var id = f._id
						var f = f.forms;
						f['token'] = expiry._id;
						f['blockId'] = req.params.blockId;
						f['buyerId'] = id;

						if (callback) {
						    res.setHeader('Content-Type', 'text/javascript');
						    res.write(callback + '(' + JSON.stringify(f) + ')');
						    res.end();

						} else {
						    res.write("Callback not set");
						    res.end();
						}
					}else{
						res.end("Form not found");
					}
				})

				var log = new ipLogger({
					ip: ip,
					origin: req.header('Origin'),
					visits: 1
				}).save()
			} else {
				entry.visits += 1
				entry.save()
				res.end("Too many requests from this ip today.")
			}
		})
	});

	/**
	 * Form submission handler.
	 */
	router.post('/submit', function(req,res){
		token.findOne({'_id':req.body.token}, function(err,obj){
			if(obj){
				token.find({ '_id':req.body.token }).remove().exec();
				var mongoose = require('mongoose');
				var formObjId = mongoose.Types.ObjectId(req.body.formId);
				var buyerObjId = mongoose.Types.ObjectId(req.body.buyerId);
				console.log('BlockID',req.body.blockId)
				var blockObjId = mongoose.Types.ObjectId(req.body.blockId);
				block.find({'_id': req.body.blockId}, function(err, block){
					console.log('Block:',block)
					buyer.findOne({'forms._id':formObjId}, function (err,buyer) {
						var bid = 0;
						var forms = buyer.forms.filter(function(form){
							if(JSON.stringify(form._id) === JSON.stringify(req.body.formId)) bid = form.bid
						})
						if(err) res.end("could not find buyer")
						buyer.credit -= bid
						buyer.save(function(err){
							if(!err){
								var a = [];
								a.push(req.body);
								var data = new submission({
									fields: a,
									bid: bid,
									_formId: formObjId,
									_buyerId: buyerObjId,
									_blockId: blockObjId,
									_publisherId: mongoose.Types.ObjectId(block[0].creator)
								});
								data.save(function (err) {
									if(err) res.end('there was a save error')

								});
							}
						})
						res.end(JSON.stringify(buyer))
					});
				})
				res.end("Successful submission in ");
			}else{
				res.end("Couldn't submit. Token has expired.");
			}
		});
	});

	return router;
}

/**
 * Get a form without geo location query.
 * @param  {String} req Http request object.
 * @return {String}     Return Promise.
 */
function getFormNoGeo(req) {
	return new Promise(function(resolve, reject){
		buyer.aggregate([
		    {$unwind: "$forms"},
		    {$project: {
		    	email: 1,
		        username : 1,
		        url: 1,
		        credit : 1,
		        forms : 1,
		        lower : {"$cond" : [{$lt: [ '$forms.bid', '$credit']}, 1, 0]}
		    }},
		    {$match:{
		    	//email: {$ne: 'jordancalder@gmail.com'},
		    	lower:1,
		    	credit:{$gte:0},
		    	'forms.fields': {$exists: true, $not: {$size: 0}},
		    	'forms.active': true
		    }},
		    { $sort: { 'forms.bid': -1 } },
		    { $limit: 1 }
		], function(err,f){
			f.length
				? resolve(f[0])
				: reject('was undefined')
		});
	})
}

/**
 * Get a form with a geo location query.
 * @param  {String} req Http request object.
 * @return {String}     Return Promise
 */
function getFormByGeo(req) {
	return new Promise(function(resolve, reject){
		var ip = process.env.PARAM1 === 'production'
		? req.header('X-Forwarded-For')
		: '67.186.210.17';
		var geo = geoip.lookup(ip);
		var original_ll = {
			latitude: geo.ll[0],
			longitude: geo.ll[1]
		}
		var distance = 4000

		buyer.aggregate([
			{
			 $geoNear: {
			    near: geo.ll,
			    maxDistance: distance,
			    distanceField: 'dist'
			 }
			},
		    {$unwind: "$forms"},
		    {$project: {
		    	email: 1,
		        username : 1,
		        url: 1,
		        credit : 1,
		        forms : 1,
		        lower : {"$cond" : [{$lt: [ '$forms.bid', '$credit']}, 1, 0]}
		    }},
		    {$match:{
		    	//email: {$ne: 'jordancalder@gmail.com'},
		    	lower:1,
		    	credit:{$gte:0},
		    	'forms.fields': {$exists: true, $not: {$size: 0}},
		    	'forms.active': true
		    }},
		    { $sort: { 'forms.bid': -1 } }
		], function(err,f){
			if(!f.length){
				reject('was undefined')
			}else{
				var largest = {
					i :  0,
					bid : 0
				};
				f.forEach(function(e,i,a){
					var ll = {
						latitude: typeof e.forms.latlong === 'undefined' ? 0 : e.forms.latlong[0],
						longitude: typeof e.forms.latlong === 'undefined' ? 0 : e.forms.latlong[1]
					}

					var inCircle = geolib.isPointInCircle(
					    ll,
					    original_ll,
					    distance
					);

					if(inCircle && e.forms.bid > largest.bid) {
						largest.bid = e.forms.bid
						largest.i = i
					}
				});
				resolve(f[largest.i])
			}
		});
	})
}

/**
 * Set up cors options. If user/block combo found, set true.
 * @param  {String}   req      Request object
 * @param  {Function} callback Cors callback
 */
var corsOptionsDelegate = function(req, callback){
  publisher.findOne({
  		url: req.header('Origin'),
  		blocks: req.params.blockId
  	},function(err,match){
    	var corsOptions;
    	if(match && match.length !== 0){
    		corsOptions = {
		    	origin:true,
		    	credentials: true
		    };
    	}else{
    		corsOptions = {
    			origin: false,
		    	credentials: false
		    };
    	}
    	callback(null, corsOptions);
  });
};