var LocalStrategy   = require('passport-local').Strategy;
var Buyer = require('../models/buyer');
var bCrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var ses = require('nodemailer-ses-transport');
var S3_KEY = 'AKIAJPISKDZOBNRUYU3Q';
var S3_SECRET = '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';
var helper = require('../utilities/helpers.js')

module.exports = function(passport){

    passport.use('buyer_signup', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, username, password, done) {

            findOrCreateUser = function(){

                Buyer.findOne({ 'username' :  username }, function(err, user) {

                    if (err){
                        console.log('Error in SignUp: '+err);
                        return done(err);
                    }

                    if (user) {
                        console.log('User already exists with username: '+username);
                        return done(null, false, req.flash('danger','User Already Exists'));
                    } else {

                        var newUser = new Buyer();

                        newUser.username = username;
                        newUser.password = createHash(password);
                        newUser.email = req.param('email');
                        newUser.firstName = req.param('firstName');
                        newUser.lastName = req.param('lastName');
                        newUser.url = helper.urlFormat(req.param('url'));
                        newUser.credit = 0;
                        newUser.verified = false;

                        crypto.randomBytes(20, function(err, buf) {
                            newUser.verifyToken = buf.toString('hex');

                            newUser.save(function(err) {
                                if (err){
                                    console.log('Error in Saving user: '+err);
                                    throw err;
                                }
                                var smtpTransport = nodemailer.createTransport(ses({
                                  accessKeyId: S3_KEY,
                                  secretAccessKey: S3_SECRET,
                                  region: 'us-west-2'
                                }));
                                var mailOptions = {
                                    to: newUser.email,
                                    from: 'info@prospector.io',
                                    subject: 'Welcome to Prospector.io',
                                    text: 'Hello '+ newUser.firstName +',\n\n' +
                                        'You recently created an account with us at prospector.io.\n\n' +
                                        'Please click this link to verify that this email address belongs to you:\n\n' +
                                        'http://' + req.headers.host + '/verify/' + newUser.verifyToken + '\n\n' +
                                        'Thanks,\n' +
                                        'The Prospector Team'
                                };
                                smtpTransport.sendMail(mailOptions, function(err) {
                                    req.flash('success', 'Success! Your registration was successful. Please check your email for a verification message.');
                                });
                                console.log('User Registration succesful');
                                return done(null, newUser);
                            });
                        });
                    }
                });
            };
            process.nextTick(findOrCreateUser);
        })
    );

    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

}