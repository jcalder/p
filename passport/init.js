var login = require('./login');
var signup = require('./signup');
var publisher_login = require('./publisher-login');
var publisher_signup = require('./publisher-signup');
var buyer_login = require('./buyer-login');
var buyer_signup = require('./buyer-signup');
var User = require('../models/user');
var Publisher = require('../models/publisher');
var Buyer = require('../models/buyer');

module.exports = function(passport){

	// Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(user, done) {
        console.log('serializing user: ');console.log(user);
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
        Publisher.findById(id, function(err,user){
            if(err) done(err);
            if(user){
                done(null,user);
            }else{
                Buyer.findById(id, function(err, user){
                    if(err) done(err);
                    done(null,user);
                });
            }
        });
    });

    // Setting up Passport Strategies for Login and SignUp/Registration
    login(passport);
    signup(passport);
    publisher_login(passport);
    publisher_signup(passport);
    buyer_signup(passport);
    buyer_login(passport);

}