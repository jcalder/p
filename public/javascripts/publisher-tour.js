$(function() {
var tour = new Tour({
	steps: [
		{
			orphan: true,
			title: "Quick Tour",
			content: "Welcome to a quick tour of Prospector.io. We'll give you a quick overview of key features you need to know to get started. You can end the tour at any point."
		},
		{
			element: "#stats",
			title: "Stats Overview",
			content: "Your homepage provides an overview of your account and gives you a report on your estimated earnings.",
			placement: "left",
			path: "/home"
		},
		{
			element: "#block-step",
			title: "Ads",
			content: "Create blocks that will allow you to advertise on your websites.",
			placement: "bottom",
			path: "/home"
		},
		{
			element: "#new-block",
			title: "Create Ad Blocks",
			content: "Here, you will create new blocks and code to paste into your sites for advertising and tracking metrics.",
			placement: "left",
			path: "/blocks"
		}
	],
	onEnd: function (tour) {
		$.get("../tour-complete", function(data, status){});
	},
	delay:500
});

tour.init();
tour.start();
})