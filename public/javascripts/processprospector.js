window.captor = (function () {
    function Captor (init) {
    	this.data_id = init.id
    }

    var captor = {
        create: function () {
        	var me = document.querySelector('script[data-id][data-name="prospectscript"]');
    	    var sid = me.getAttribute('data-id');
        	var initialize = {
        		id: sid
        	}
        	return new Captor(initialize);
        }
    };

    Captor.prototype.fadeIn = function (el) {
      el.style.opacity = 0;
      el.style.display = 'block';
      var last = +new Date();
      var tick = function() {
        el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
        last = +new Date();
        if (+el.style.opacity < 1) {
          (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16)
        }
      };
      tick();
    }

    Captor.prototype.setCookie = function (c_name, value, exhours) {
        var exdate = new Date();
        exdate.setHours(exdate.getHours() + exhours);
        var c_value = escape(value) + ((exhours == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    }

    Captor.prototype.fadeOut = function(elem) {
        var fadeTarget = elem
        var fadeEffect = setInterval(function () {
            if (!fadeTarget.style.opacity) {
                fadeTarget.style.opacity = 1;
            }
            if (fadeTarget.style.opacity < 0) {
                fadeTarget.style.display = 'none';
                clearInterval(fadeEffect);
            } else {
                fadeTarget.style.opacity -= 0.1;
            }
        }, 50);
    };

    Captor.prototype.build = function () {
        console.log('building...')
        var wayBack = this.backContain();
    	var interstitial = this.backdrop();
        var container = this.container();
        var close = this.closeModal();
        wayBack.appendChild(interstitial);
        wayBack.appendChild(container);
        wayBack.setAttribute('style','opacity:0;display:none;font-family: Arial, Helvetica, sans-serif;');
        container.appendChild(close);
        console.log('wayBack',wayBack)
        document.body.insertBefore(wayBack,document.body.firstChild);
    };

    Captor.prototype.backContain = function(){
        console.log('backContain')
    	var b = document.createElement("div");
		b.setAttribute('style',"position:relative;z-index:100;font-family: Arial;");
		b.setAttribute('id',"wayBack");
		return b;
    };

    Captor.prototype.backdrop = function(){
        console.log('backdrop')
    	var b = document.createElement("div");
		b.setAttribute('style',"position:fixed;width:100%;height:100%;background: rgba(0,0,0,.5);z-index: 101;");
		b.setAttribute('id',"backdrop");
        b.setAttribute("onclick","javascript:window.builder.fadeOut(document.getElementById('wayBack'));");
		return b;
    };

    Captor.prototype.container = function(){
        console.log('container')
    	var c = document.createElement("div");
        c.setAttribute('style',"position:fixed;background:#F5F5F5;min-height:100px;width: 480px;margin: auto;top: 50%;left:50%;margin-top:-176px;margin-left:-240px;padding:15px;z-index:102;webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);box-shadow: 0 5px 15px rgba(0,0,0,.5);");
        var fi = document.createElement('iframe');
        fi.setAttribute('style', "width:495px; min-height:305px;border:none;")
        fi.src = 'http://api.prospector.io/iframe/'+this.data_id;
        c.appendChild(fi)
    	return c;
    };

    Captor.prototype.closeModal = function(){
    	var a = document.createElement("a");
    	var s = document.createElement("span");
    	var c = document.createTextNode("Close");
        a.setAttribute('style',"color:#CE4547;float:right;cursor:pointer");
        a.setAttribute("onclick","javascript:window.builder.fadeOut(document.getElementById('wayBack'));");
    	a.appendChild(s);
    	s.appendChild(c);
    	return a;
    };

    Captor.prototype.closeOut = function(){
        document.getElementById("wayBack").style.display = 'none';
    };

    return captor;
}());

window.addEventListener( "message",
  function (e) {
        if(e.data && e.origin === 'http://api.prospector.io'){
            console.log('data: ', e)
            console.log('message: ',e.data)
        	if(e.data === 'submitted'){
        		window.builder.fadeOut(document.getElementById('wayBack'))
        	}else{
		        window.builder.fadeIn(document.getElementById('wayBack'));
        	}
  		}
  },
false);

console.log('reading script...')
window.addEventListener("load", function(event) {
    console.log("content loaded...")
    window.builder = captor.create();
    var body = document.getElementsByTagName("body")[0];
    console.log(body)
    //if(document.cookie.indexOf(e.data) == -1){
        if(document.cookie.indexOf(window.builder.data_id) == -1){
            console.log('builder',window.builder)
            //window.builder.setCookie(e.data,"set",14);
            window.builder.setCookie(window.builder.data_id,"set",1);
            window.builder.build()
        }
    //}
},
false);