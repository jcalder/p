window.captor = (function () {
    function Captor () {

    }

    var captor = {
        create: function () {
        	return new Captor();
        }
    };

    Captor.prototype.fadeIn = function (el) {
      el.style.opacity = 0;

      var last = +new Date();
      var tick = function() {
        el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
        last = +new Date();

        if (+el.style.opacity < 1) {
          (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16)
        }
      };

      tick();
    }

    Captor.prototype.setCookie = function (c_name, value, exhours) {
        var exdate = new Date();
        var c_value = escape(value) + ((exhours == null) ? "" : "; expires=" + exdate.toUTCString());
        console.log(c_value);

        exdate.setHours(exdate.getHours() + exhours);

        var c_value = escape(value) + ((exhours == null) ? "" : "; expires=" + exdate.toUTCString());
        console.log(c_value);

        document.cookie = c_name + "=" + c_value;
    }

    Captor.prototype.fadeOut = function(elem) {
        var fadeTarget = elem
        var fadeEffect = setInterval(function () {
            if (!fadeTarget.style.opacity) {
                fadeTarget.style.opacity = 1;
            }
            if (fadeTarget.style.opacity < 0) {
                fadeTarget.style.display = none;
                clearInterval(fadeEffect);
            } else {
                fadeTarget.style.opacity -= 0.1;
            }
        }, 50);
    };

    Captor.prototype.build = function (json) {
        //CREATE BACKDROP WITH CLOSE FUNCTION
        var wayBack = this.backContain();
    	var interstitial = this.backdrop();
        //CREATE DIV CONTAINER
        var container = this.container();
        var image = this.setImg(json);
        //CREATE TITLE
        var title = this.setTitle(json);
        //CREATE CALL TO ACTION
        var cta = this.callToAction(json);
        //CREATE FORM WITH INPUTS
        var form = this.capture(json);
        var close = this.closeModal();
        wayBack.appendChild(interstitial);
        wayBack.appendChild(container);
        wayBack.setAttribute('style','opacity:0;');
        wayBack.setAttribute('style','font-family:Arial, Helvetica, sans-serif;');
        container.appendChild(image);
        container.appendChild(title);
        container.appendChild(cta);
        container.appendChild(form);
        container.appendChild(close);
        var body = document.getElementsByTagName("body")[0];
        if(document.cookie.indexOf(json._id) == -1){
            if(document.cookie.indexOf(json.blockId) == -1){
                this.setCookie(json._id,"set",14);
                this.setCookie(json.blockId,"set",1);
                document.body.insertBefore(wayBack,body.firstChild);
                this.fadeIn(document.getElementById('wayBack'));
            }
        }
    };

    Captor.prototype.backContain = function(){
    	var b = document.createElement("div");
		b.setAttribute('style',"position:relative;z-index:100;font-family: Arial;");
		b.setAttribute('id',"wayBack");
		return b;
    };

    Captor.prototype.backdrop = function(){
    	var b = document.createElement("div");
		b.setAttribute('style',"position:fixed;width:100%;height:100%;background: rgba(0,0,0,.5);z-index: 101;");
		b.setAttribute('id',"backdrop");
        b.setAttribute("onclick","javascript:n.fadeOut(document.getElementById('wayBack'));");
		return b;
    };

    Captor.prototype.container = function(){
    	var c = document.createElement("div");
        c.setAttribute('style',"position:fixed;background:#F5F5F5;min-height:100px;width: 480px;margin: auto;top: 30%;left:50%;margin-left:-240px;padding:15px;z-index:102;webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);box-shadow: 0 5px 15px rgba(0,0,0,.5);");
    	return c;
    };

    Captor.prototype.setImg = function (json){
        var i = document.createElement("img");
        i.setAttribute('style',"width:100%;");
        i.setAttribute('src',"http://cdn.drunkcandy.com/"+json.logo);
        return i;
    }

    Captor.prototype.setTitle = function(json){
    	var t = document.createElement("h1");
    	t.setAttribute('style',"margin-top:10px");
        var inside = document.createTextNode(json.cta);
    	t.appendChild(inside);
    	return t;
    };

    Captor.prototype.callToAction = function(json){
    	var cta = document.createElement("p");
        var inside = document.createTextNode(json.copy);
    	cta.appendChild(inside);
    	return cta;
    };

    Captor.prototype.capture = function(json){
    	console.log(json);
    	var f = document.createElement("form");
    	var hidden = document.createElement("input");
        var token = document.createElement("input");
        var blockId = document.createElement("input");
        var buyerId = document.createElement("input");
    	var s = document.createElement("button");
    	var c = document.createTextNode("Submit");
        f.setAttribute('onsubmit',"AJAXPost(this); n.fadeOut(document.getElementById('wayBack')); return false;");
        f.setAttribute('id',"f");
        f.setAttribute('style', "margin-bottom:10px;");
    	hidden.setAttribute('type',"hidden");
        hidden.setAttribute('name',"formId");
    	hidden.setAttribute('value',json._id);
        token.setAttribute('type',"hidden");
        token.setAttribute('name',"token");
        token.setAttribute('value',json.token._id);
        blockId.setAttribute('type',"hidden");
        blockId.setAttribute('name',"blockId");
        blockId.setAttribute('value',json.blockId);
        buyerId.setAttribute('type',"hidden");
        buyerId.setAttribute('name',"buyerId");
        buyerId.setAttribute('value',json.buyerId);
    	s.setAttribute('type',"submit");
    	s.setAttribute('style',"color: #333;background-color: #fff;border-color: #ccc;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;-ms-touch-action: manipulation;touch-action: manipulation;cursor: pointer;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;background-image: none;border: 1px solid transparent;border-radius: 4px;");

    	for(var i = 0; i < json.fields[0].fields.length ; i++){
    		var field = this.buildInput(json.fields[0].fields[i]);
    		console.log(field);
    		f.appendChild(field);
    	}
    	s.appendChild(c);
    	f.appendChild(hidden);
        f.appendChild(token);
        f.appendChild(blockId);
        f.appendChild(buyerId);
        f.appendChild(s);
    	return f;
    };

    Captor.prototype.closeModal = function(){
    	var a = document.createElement("a");
    	var s = document.createElement("span");
    	var c = document.createTextNode("Close");
        a.setAttribute('style',"color:#CE4547;float:right;");
        a.setAttribute("onclick","javascript:n.fadeOut(document.getElementById('wayBack'));");
    	a.appendChild(s);
    	s.appendChild(c);
    	return a;
    };

    Captor.prototype.buildInput = function(field){
    	var el;
    	switch(field.field_type) {
		    case "text":
		    	el = document.createElement("input");
		    	el.setAttribute('type',"text");
                el.setAttribute('name',"fields['"+field.label+"']");
		    	el.setAttribute('placeholder',field.label);
                el.setAttribute('style',"margin-right:5px;height: 20px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;-o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;");
		    	break;
		    case "email":
		    	el = document.createElement("input");
		    	el.setAttribute('type',"email");
                el.setAttribute('name',"fields['"+field.label+"']");
		    	el.setAttribute('placeholder',field.label);
                el.setAttribute('style',"margin-right:5px;height: 20px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;-o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;");
		    	break;
		    case "checkboxes":
		    	for(var j = 0; j < field.field_options.options.length; j++){

		    	}
		        break;
		    case "paragraph":
		    	el = document.createElement("textarea");
                el.setAttribute('name',"fields['"+field.label+"']");
		    	el.setAttribute('placeholder',field.label);
		    	break;
		    case "radio":
		    	//STUFF
		    	break;
		    case "date":
		    	el = document.createElement("input");
		    	el.setAttribute('type',"date");
                el.setAttribute('name',"fields['"+field.label+"']");
		    	break;
		    case "dropdown":
		    	el = document.createElement("select");
                el.setAttribute('name',"fields['"+field.label+"']");
		    	for(var j = 0; j < field.field_options.options.length; j++){
		    		var op = document.createElement("option");
		    		op.setAttribute('value',field.field_options.options[j].label);
		    		el.appendChild(op);
		    	}
		    	break;
		    case "section_break":
		    	el = document.createElement("hr");
		    	break;
		    case "number":
		        break;
		    case "website":
		        break;
		    case "price":
		    	break;
		    case "address":
		    	break;
		    default:
		}
		return el;
    };

    Captor.prototype.closeOut = function(){
        document.getElementById("wayBack").style.display = 'none';
    };

    return captor;
}());

function AJAXPost(formSub) {
    console.log('submitting...');

    var http = new XMLHttpRequest();
    http.open("POST", "http://api.drunkcandy.com/submit", true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    var params = ""; // probably use document.getElementById(...).value
    var elements = formSub.elements;
    for (i=0; i<elements.length; i++){
        console.log(elements[i]);
        if(elements[i].name != '' && elements[i].value != '')
            params += elements[i].name + "=" + elements[i].value + "&";
    }
    http.send(params);
    http.onload = function() {
        console.log(http.responseText);
    }
}