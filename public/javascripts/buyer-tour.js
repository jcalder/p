$(function() {
var tour = new Tour({
	steps: [
		{
			orphan: true,
			title: "Quick Tour",
			content: "Welcome to a quick tour of Prospector.io. We'll give you a quick overview of key features you need to know to get started. You can end the tour at any point."
		},
		{
			element: "#stats",
			title: "Stats Overview",
			content: "Your homepage provides an overview of your account and gives you stats on your campaigns and lead submissions.",
			placement: "left",
			path: "/home"
		},
		{
			element: "#campaigns",
			title: "Campaigns",
			content: "Find your current campaigns and build new ones here.",
			placement: "bottom",
			path: "/home"
		},
		{
			element: "#submissions",
			title: "Submissions",
			content: "You'll find your lead subissions per campaign here.",
			placement: "bottom",
			path: "/home"
		},
		{
			element: "#payment-form",
			title: "Fund account",
			content: "Before your campaigns can run live, you must fund your account. That's everything you need to know to get started. So easy, right?",
			path: "/fund"
		}
	],
	onEnd: function (tour) {
		$.get("../tour-complete", function(data, status){});
	},
	delay:500
});

tour.init();
tour.start();
})