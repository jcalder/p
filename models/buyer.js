var mongoose = require('mongoose');
require('./form.js');
 
module.exports = mongoose.model('Buyer',{
	username: String,
    password: String,
    email: String,
    url: String,
    id: String,
    firstName: String,
	lastName: String,
	credit: Number,
	forms:[mongoose.model('Form').schema],
	resetPasswordToken: {type: String, expires: 3600},
	verified: Boolean,
	verifyToken: String,
	takenTour: Boolean
});