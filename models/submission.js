var mongoose = require('mongoose')
var dataTables = require('mongoose-datatables')
var Schema = mongoose.Schema

var Submission = new Schema({
	createdAt: {type: Date, default: Date.now},
	bid: Number,
	_formId: {type: mongoose.Schema.Types.ObjectId, ref: 'Form'},
	_blockId: {type: mongoose.Schema.Types.ObjectId, ref: 'Block'},
	_buyerId: {type: mongoose.Schema.Types.ObjectId, ref: 'Buyer'},
	_publisherId: {type: mongoose.Schema.Types.ObjectId, ref: 'Publisher'},
	fields: [],
}).plugin(dataTables)

module.exports = mongoose.model('Submission', Submission);