var mongoose = require('mongoose');

module.exports = mongoose.model('Form',{
	cta: String,
	copy: String,
	logo: String,
	keywords: [],
	bid: Number,
	creator: [],
	fields: [],
	latlong: { type: [Number], index: '2d' },
	country: String,
	region: String,
	city: String,
	active: Boolean,
	dailyMax: Number,
	dailyCount: Number,
});