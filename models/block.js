var mongoose = require('mongoose')
var dataTables = require('mongoose-datatables')
var Schema = mongoose.Schema

var Block = new Schema({
	creator: {type: mongoose.Schema.Types.ObjectId, ref: 'Publisher'},
	name: String,
    status: String,
    description: { type: String, maxlength: 255 },
    enabled: Boolean
}).plugin(dataTables)

module.exports = mongoose.model('Block', Block);