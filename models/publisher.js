var mongoose = require('mongoose');
 
module.exports = mongoose.model('Publisher',{
    username: String,
    password: String,
    email: String,
    url: String,
    firstName: String,
	lastName: String,
	blocks: [{type:mongoose.Schema.Types.ObjectId, ref: 'Block'}],
	resetPasswordToken: {type: String, expires: 300},
	verified: Boolean,
	verifyToken: String,
	takenTour: Boolean
});