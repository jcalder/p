var mongoose = require('mongoose');

module.exports = mongoose.model('Token',{
	createdAt: {type: Date, expires: 300, default: Date.now},
});