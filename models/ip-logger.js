var mongoose = require('mongoose')
var Schema = mongoose.Schema

var IpLog = new Schema({
	ip: String,
	origin: String,
	date: {type:Date, default: Date.now},
	visits: Number
})

module.exports = mongoose.model('IpLog', IpLog);