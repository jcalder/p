var request = require('supertest')
var express = require('express')
var passport = require('passport')
var app = express();
var routes = require('../routes/index')(passport);

app.set('view engine', 'jade');
app.use('/', routes);

describe('GET /', function(){
	it('should respond with 200', function(done){
		request(app)
			.get('/')
			.set('Accept', 'text/html')
			.expect('Content-Type', 'text/html; charset=utf-8')
			.expect(200, done);
	})
})

describe('GET /about', function(){
	it('should respond with 200', function(done){
		request(app)
			.get('/about')
			.set('Accept', 'text/html')
			.expect('Content-Type', 'text/html; charset=utf-8')
			.expect(200, done);
	})
})

describe('GET /contact', function(){
	it('should respond with 200', function(done){
		request(app)
			.get('/contact')
			.set('Accept', 'text/html')
			.expect('Content-Type', 'text/html; charset=utf-8')
			.expect(200, done);
	})
})
